#!/usr/bin/env python
# -*- coding:utf-8 -*-

"""Genera tu cadena de caracteres a partir de un algoritmo genético de forma aleatoria.

Flags:
-t --> Target: Elige la cadena de caracteres a la que se intentará evolucionar. (por defecto "Hello, World!")
-p --> Población: Elige la cantidad de individuos que habrá en cada generación. (por defecto 18)
-m --> Ritmo de mutación: Elige el porcentaje entre 0 y 1 de individuos que mutan. (por defecto 0.05)
Los 3 flags son opcionales"""

import random, sys

class Agent:
	target = sys.argv[sys.argv.index('-t')+1] if '-t' in sys.argv else "Hello, World!"

	def __init__(self, word=None):
		if word == None:
			self.word=''
			for i in range(len(self.target)):
				self.word+=chr(random.randint(32,122))
		else:
			self.word=word

	def __repr__(self):
		return self.word

	def __add__(self, agent2):
		return Agent.breeding(self, agent2)

	@property
	def fitness(self):
		fitness=0
		for i in range(len(self.word)):
			if self.word[i] == self.target[i]:
				fitness+=1
		return fitness

	def mutate(self, mutation_rate=0.05):
		assert mutation_rate >= 0 and mutation_rate <= 1, "El porcentaje de mutación debe estar entre 0 y 1"
		if random.random() <= mutation_rate:
			position = random.randint(0,len(self.word)+1)
			new_word='' 
			for i in range(len(self.word)):
				if i == position:
					new_word += chr(random.randint(32,122))
				else:
					new_word+=self.word[i]
			self.word=new_word

	@staticmethod
	def breeding(agent1, agent2):
		breaking_point=random.randint(1,len(agent1.word))
		child1=Agent(agent1.word[:breaking_point]+agent2.word[breaking_point:])
		child2=Agent(agent2.word[:breaking_point]+agent1.word[breaking_point:])
		return child1, child2

	@staticmethod
	def selection(agents):
		fitness_scores = [agent.fitness+1 for agent in agents]
		total = sum(fitness_scores)
		selected_ones = []
		while len(selected_ones) < 2:
			rdn = random.randint(1,total)
			for i in range(len(agents)):
				if rdn <= sum(fitness_scores[:i+1]) and agents[i] not in selected_ones:
					selected_ones.append(agents[i])
					break
		return selected_ones

def generate_population(people=18):
	assert people > 2, "Población insuficiente"
	agents=[]
	i=0
	while i < people:
		i+=1
		agents.append(Agent())
	return agents

def main():
	if '-p' in sys.argv: agents=generate_population(people = int(sys.argv[sys.argv.index('-p')+1]))
	else: agents=generate_population()
	fitness_scores = [agent.fitness for agent in agents]
	iterations=1
	success = False
	while len(Agent.target) not in fitness_scores:
		print('Generación número', iterations)
		selected_ones = Agent.selection(agents)
		new_gen = Agent.breeding(*selected_ones)
		print('---', new_gen[0],'---\n---', new_gen[1], '---')
		agents.append(new_gen[0])
		agents.append(new_gen[1])
		agents.pop(fitness_scores.index(min(fitness_scores)))
		fitness_scores.pop(fitness_scores.index(min(fitness_scores)))
		agents.pop(fitness_scores.index(min(fitness_scores)))
		for a in agents:
			if '-m' in sys.argv: a.mutate(mutation_rate = float(sys.argv[sys.argv.index('-m')+1]))
			else: a.mutate()
		fitness_scores = [agent.fitness for agent in agents]
		if iterations >= 50000: break
		iterations+=1
	else: success = True

	if success:
		winner = agents[fitness_scores.index(len(Agent.target))]
		print('Felicidades!!!\nSe ha alcanzado {} en {} generaciones.'.format(winner,iterations))
	else:
		print('Tras {} generaciones no se ha alcanzado el objetivo.\nEstos son los resultados:'.format(iterations))
		for agent in agents: print(agent)

if __name__ == '__main__':
	main()
